﻿using System;
using Newtonsoft.Json;
using Supernova.SemanticVersioning;
using Xunit;

namespace SemanticVersioningTests
{
    public class SemanticVersionTests
    {
        private readonly SemanticVersion _versionA = new SemanticVersion(42, 233, 17, "-alpha");
        private readonly SemanticVersion _versionB = new SemanticVersion(42, 233, 17, "-alpha.1");
        private readonly SemanticVersion _versionC = new SemanticVersion(42, 233, 17, "-alpha.beta");
        private readonly SemanticVersion _versionD = new SemanticVersion(42, 233, 17, "-beta");
        private readonly SemanticVersion _versionE = new SemanticVersion(42, 233, 17, "-beta.2");
        private readonly SemanticVersion _versionF = new SemanticVersion(42, 233, 17, "-beta.11");
        private readonly SemanticVersion _versionG = new SemanticVersion(42, 233, 17, "-rc.1");
        private readonly SemanticVersion _versionGBis = new SemanticVersion(42, 233, 17, "-rc.1", "+foo.bar.baz");
        private readonly SemanticVersion _versionH = new SemanticVersion(42, 233, 17);
        private readonly SemanticVersion _versionI = new SemanticVersion(54, 0, 0);
        private readonly SemanticVersion _versionJ = new SemanticVersion(54, 12, 0);
        private readonly SemanticVersion _versionK = new SemanticVersion(54, 12, 3);
        private readonly SemanticVersion _versionL = new SemanticVersion(109, 0, 0);

        [Fact]
        public void SemanticVersion_MajorIncremented_DoesNotChangeCurrentInstance()
        {
            SemanticVersion result = _versionGBis.MajorIncremented();

            Assert.NotSame(result, _versionGBis);
            Assert.Equal(new SemanticVersion(42, 233, 17, "-rc.1", "+foo.bar.baz"), _versionGBis);
        }

        [Fact]
        public void SemanticVersion_MajorIncremented_ReturnsNewMajorIncrementedVersion()
        {
            SemanticVersion versionA2 = _versionA.MajorIncremented();
            SemanticVersion versionG2 = _versionG.MajorIncremented();
            SemanticVersion versionGBis2 = _versionGBis.MajorIncremented();
            SemanticVersion versionK2 = _versionK.MajorIncremented();

            Assert.Equal(new SemanticVersion(43, 0, 0, "-alpha"), versionA2);
            Assert.Equal(new SemanticVersion(43, 0, 0, "-rc.1"), versionG2);
            Assert.Equal(new SemanticVersion(43, 0, 0, "-rc.1", "+foo.bar.baz"), versionGBis2);
            Assert.Equal(new SemanticVersion(55, 0, 0), versionK2);
        }

        [Fact]
        public void SemanticVersion_MinorIncremented_DoesNotChangeCurrentInstance()
        {
            SemanticVersion result = _versionGBis.MinorIncremented();

            Assert.NotSame(result, _versionGBis);
            Assert.Equal(new SemanticVersion(42, 233, 17, "-rc.1", "+foo.bar.baz"), _versionGBis);
        }

        [Fact]
        public void SemanticVersion_MinorIncremented_ReturnsNewMinorIncrementedVersion()
        {
            SemanticVersion versionA2 = _versionA.MinorIncremented();
            SemanticVersion versionG2 = _versionG.MinorIncremented();
            SemanticVersion versionGBis2 = _versionGBis.MinorIncremented();
            SemanticVersion versionK2 = _versionK.MinorIncremented();

            Assert.Equal(new SemanticVersion(42, 234, 0, "-alpha"), versionA2);
            Assert.Equal(new SemanticVersion(42, 234, 0, "-rc.1"), versionG2);
            Assert.Equal(new SemanticVersion(42, 234, 0, "-rc.1", "+foo.bar.baz"), versionGBis2);
            Assert.Equal(new SemanticVersion(54, 13, 0), versionK2);
        }

        [Fact]
        public void SemanticVersion_PatchIncremented_DoesNotChangeCurrentInstance()
        {
            SemanticVersion result = _versionGBis.PatchIncremented();

            Assert.NotSame(result, _versionGBis);
            Assert.Equal(new SemanticVersion(42, 233, 17, "-rc.1", "+foo.bar.baz"), _versionGBis);
        }

        [Fact]
        public void SemanticVersion_PatchIncremented_ReturnsNewPatchIncrementedVersion()
        {
            SemanticVersion versionA2 = _versionA.PatchIncremented();
            SemanticVersion versionG2 = _versionG.PatchIncremented();
            SemanticVersion versionGBis2 = _versionGBis.PatchIncremented();
            SemanticVersion versionK2 = _versionK.PatchIncremented();

            Assert.Equal(new SemanticVersion(42, 233, 18, "-alpha"), versionA2);
            Assert.Equal(new SemanticVersion(42, 233, 18, "-rc.1"), versionG2);
            Assert.Equal(new SemanticVersion(42, 233, 18, "-rc.1", "+foo.bar.baz"), versionGBis2);
            Assert.Equal(new SemanticVersion(54, 12, 4), versionK2);
        }

        [Fact]
        public void SemanticVersion_ToString_ReturnsStringRepresentation()
        {
            Assert.Equal("42.233.17-alpha", _versionA.ToString());
            Assert.Equal("42.233.17-alpha.1", _versionB.ToString());
            Assert.Equal("42.233.17-alpha.beta", _versionC.ToString());
            Assert.Equal("42.233.17-beta", _versionD.ToString());
            Assert.Equal("42.233.17-beta.2", _versionE.ToString());
            Assert.Equal("42.233.17-beta.11", _versionF.ToString());
            Assert.Equal("42.233.17-rc.1", _versionG.ToString());
            Assert.Equal("42.233.17-rc.1+foo.bar.baz", _versionGBis.ToString());
            Assert.Equal("42.233.17", _versionH.ToString());
            Assert.Equal("54.0.0", _versionI.ToString());
            Assert.Equal("54.12.0", _versionJ.ToString());
            Assert.Equal("54.12.3", _versionK.ToString());
            Assert.Equal("109.0.0", _versionL.ToString());
        }

        [Fact]
        public void SemanticVersion_CompareTo_ReturnsComparisonResult()
        {
            Assert.True(_versionA.CompareTo(_versionA) == 0);
            Assert.True(_versionA.CompareTo(_versionB) < 0);
            Assert.True(_versionA.CompareTo(_versionC) < 0);
            Assert.True(_versionA.CompareTo(_versionD) < 0);
            Assert.True(_versionA.CompareTo(_versionE) < 0);
            Assert.True(_versionA.CompareTo(_versionF) < 0);
            Assert.True(_versionA.CompareTo(_versionG) < 0);
            Assert.True(_versionA.CompareTo(_versionH) < 0);
            Assert.True(_versionA.CompareTo(_versionI) < 0);
            Assert.True(_versionA.CompareTo(_versionJ) < 0);
            Assert.True(_versionA.CompareTo(_versionK) < 0);
            Assert.True(_versionA.CompareTo(_versionL) < 0);

            Assert.True(_versionB.CompareTo(_versionA) > 0);
            Assert.True(_versionB.CompareTo(_versionB) == 0);
            Assert.True(_versionB.CompareTo(_versionC) < 0);
            Assert.True(_versionB.CompareTo(_versionD) < 0);
            Assert.True(_versionB.CompareTo(_versionE) < 0);
            Assert.True(_versionB.CompareTo(_versionF) < 0);
            Assert.True(_versionB.CompareTo(_versionG) < 0);
            Assert.True(_versionB.CompareTo(_versionH) < 0);
            Assert.True(_versionB.CompareTo(_versionI) < 0);
            Assert.True(_versionB.CompareTo(_versionJ) < 0);
            Assert.True(_versionB.CompareTo(_versionK) < 0);
            Assert.True(_versionB.CompareTo(_versionL) < 0);

            Assert.True(_versionC.CompareTo(_versionA) > 0);
            Assert.True(_versionC.CompareTo(_versionB) > 0);
            Assert.True(_versionC.CompareTo(_versionC) == 0);
            Assert.True(_versionC.CompareTo(_versionD) < 0);
            Assert.True(_versionC.CompareTo(_versionE) < 0);
            Assert.True(_versionC.CompareTo(_versionF) < 0);
            Assert.True(_versionC.CompareTo(_versionG) < 0);
            Assert.True(_versionC.CompareTo(_versionH) < 0);
            Assert.True(_versionC.CompareTo(_versionI) < 0);
            Assert.True(_versionC.CompareTo(_versionJ) < 0);
            Assert.True(_versionC.CompareTo(_versionK) < 0);
            Assert.True(_versionC.CompareTo(_versionL) < 0);

            Assert.True(_versionD.CompareTo(_versionA) > 0);
            Assert.True(_versionD.CompareTo(_versionB) > 0);
            Assert.True(_versionD.CompareTo(_versionC) > 0);
            Assert.True(_versionD.CompareTo(_versionD) == 0);
            Assert.True(_versionD.CompareTo(_versionE) < 0);
            Assert.True(_versionD.CompareTo(_versionF) < 0);
            Assert.True(_versionD.CompareTo(_versionG) < 0);
            Assert.True(_versionD.CompareTo(_versionH) < 0);
            Assert.True(_versionD.CompareTo(_versionI) < 0);
            Assert.True(_versionD.CompareTo(_versionJ) < 0);
            Assert.True(_versionD.CompareTo(_versionK) < 0);
            Assert.True(_versionD.CompareTo(_versionL) < 0);

            Assert.True(_versionE.CompareTo(_versionA) > 0);
            Assert.True(_versionE.CompareTo(_versionB) > 0);
            Assert.True(_versionE.CompareTo(_versionC) > 0);
            Assert.True(_versionE.CompareTo(_versionD) > 0);
            Assert.True(_versionE.CompareTo(_versionE) == 0);
            Assert.True(_versionE.CompareTo(_versionF) < 0);
            Assert.True(_versionE.CompareTo(_versionG) < 0);
            Assert.True(_versionE.CompareTo(_versionH) < 0);
            Assert.True(_versionE.CompareTo(_versionI) < 0);
            Assert.True(_versionE.CompareTo(_versionJ) < 0);
            Assert.True(_versionE.CompareTo(_versionK) < 0);
            Assert.True(_versionE.CompareTo(_versionL) < 0);

            Assert.True(_versionF.CompareTo(_versionA) > 0);
            Assert.True(_versionF.CompareTo(_versionB) > 0);
            Assert.True(_versionF.CompareTo(_versionC) > 0);
            Assert.True(_versionF.CompareTo(_versionD) > 0);
            Assert.True(_versionF.CompareTo(_versionE) > 0);
            Assert.True(_versionF.CompareTo(_versionF) == 0);
            Assert.True(_versionF.CompareTo(_versionG) < 0);
            Assert.True(_versionF.CompareTo(_versionH) < 0);
            Assert.True(_versionF.CompareTo(_versionI) < 0);
            Assert.True(_versionF.CompareTo(_versionJ) < 0);
            Assert.True(_versionF.CompareTo(_versionK) < 0);
            Assert.True(_versionF.CompareTo(_versionL) < 0);

            Assert.True(_versionG.CompareTo(_versionA) > 0);
            Assert.True(_versionG.CompareTo(_versionB) > 0);
            Assert.True(_versionG.CompareTo(_versionC) > 0);
            Assert.True(_versionG.CompareTo(_versionD) > 0);
            Assert.True(_versionG.CompareTo(_versionE) > 0);
            Assert.True(_versionG.CompareTo(_versionF) > 0);
            Assert.True(_versionG.CompareTo(_versionG) == 0);
            Assert.True(_versionG.CompareTo(_versionH) < 0);
            Assert.True(_versionG.CompareTo(_versionI) < 0);
            Assert.True(_versionG.CompareTo(_versionJ) < 0);
            Assert.True(_versionG.CompareTo(_versionK) < 0);
            Assert.True(_versionG.CompareTo(_versionL) < 0);

            Assert.True(_versionH.CompareTo(_versionA) > 0);
            Assert.True(_versionH.CompareTo(_versionB) > 0);
            Assert.True(_versionH.CompareTo(_versionC) > 0);
            Assert.True(_versionH.CompareTo(_versionD) > 0);
            Assert.True(_versionH.CompareTo(_versionE) > 0);
            Assert.True(_versionH.CompareTo(_versionF) > 0);
            Assert.True(_versionH.CompareTo(_versionG) > 0);
            Assert.True(_versionH.CompareTo(_versionH) == 0);
            Assert.True(_versionH.CompareTo(_versionI) < 0);
            Assert.True(_versionH.CompareTo(_versionJ) < 0);
            Assert.True(_versionH.CompareTo(_versionK) < 0);
            Assert.True(_versionH.CompareTo(_versionL) < 0);

            Assert.True(_versionI.CompareTo(_versionA) > 0);
            Assert.True(_versionI.CompareTo(_versionB) > 0);
            Assert.True(_versionI.CompareTo(_versionC) > 0);
            Assert.True(_versionI.CompareTo(_versionD) > 0);
            Assert.True(_versionI.CompareTo(_versionE) > 0);
            Assert.True(_versionI.CompareTo(_versionF) > 0);
            Assert.True(_versionI.CompareTo(_versionG) > 0);
            Assert.True(_versionI.CompareTo(_versionH) > 0);
            Assert.True(_versionI.CompareTo(_versionI) == 0);
            Assert.True(_versionI.CompareTo(_versionJ) < 0);
            Assert.True(_versionI.CompareTo(_versionK) < 0);
            Assert.True(_versionI.CompareTo(_versionL) < 0);

            Assert.True(_versionJ.CompareTo(_versionA) > 0);
            Assert.True(_versionJ.CompareTo(_versionB) > 0);
            Assert.True(_versionJ.CompareTo(_versionC) > 0);
            Assert.True(_versionJ.CompareTo(_versionD) > 0);
            Assert.True(_versionJ.CompareTo(_versionE) > 0);
            Assert.True(_versionJ.CompareTo(_versionF) > 0);
            Assert.True(_versionJ.CompareTo(_versionG) > 0);
            Assert.True(_versionJ.CompareTo(_versionH) > 0);
            Assert.True(_versionJ.CompareTo(_versionI) > 0);
            Assert.True(_versionJ.CompareTo(_versionJ) == 0);
            Assert.True(_versionJ.CompareTo(_versionK) < 0);
            Assert.True(_versionJ.CompareTo(_versionL) < 0);

            Assert.True(_versionK.CompareTo(_versionA) > 0);
            Assert.True(_versionK.CompareTo(_versionB) > 0);
            Assert.True(_versionK.CompareTo(_versionC) > 0);
            Assert.True(_versionK.CompareTo(_versionD) > 0);
            Assert.True(_versionK.CompareTo(_versionE) > 0);
            Assert.True(_versionK.CompareTo(_versionF) > 0);
            Assert.True(_versionK.CompareTo(_versionG) > 0);
            Assert.True(_versionK.CompareTo(_versionH) > 0);
            Assert.True(_versionK.CompareTo(_versionI) > 0);
            Assert.True(_versionK.CompareTo(_versionJ) > 0);
            Assert.True(_versionK.CompareTo(_versionK) == 0);
            Assert.True(_versionK.CompareTo(_versionL) < 0);

            Assert.True(_versionL.CompareTo(_versionA) > 0);
            Assert.True(_versionL.CompareTo(_versionB) > 0);
            Assert.True(_versionL.CompareTo(_versionC) > 0);
            Assert.True(_versionL.CompareTo(_versionD) > 0);
            Assert.True(_versionL.CompareTo(_versionE) > 0);
            Assert.True(_versionL.CompareTo(_versionF) > 0);
            Assert.True(_versionL.CompareTo(_versionG) > 0);
            Assert.True(_versionL.CompareTo(_versionH) > 0);
            Assert.True(_versionL.CompareTo(_versionI) > 0);
            Assert.True(_versionL.CompareTo(_versionJ) > 0);
            Assert.True(_versionL.CompareTo(_versionK) > 0);
            Assert.True(_versionL.CompareTo(_versionL) == 0);

            Assert.True(_versionG.CompareTo(_versionGBis) == 0);
            Assert.True(_versionGBis.CompareTo(_versionG) == 0);

            Assert.True(_versionA.CompareTo(null) > 0);
        }

        [Fact]
        public void SemanticVersion_Equals_ReturnsComparisonResult()
        {
            Assert.Equal(_versionA, _versionA);
            Assert.NotEqual(_versionA, _versionB);
            Assert.NotEqual(_versionA, _versionH);

            Assert.NotEqual(_versionH, _versionA);
            Assert.NotEqual(_versionH, _versionI);

            Assert.Equal(_versionG, _versionG);
            Assert.Equal(_versionG, _versionGBis);
            Assert.Equal(_versionGBis, _versionGBis);

            Assert.NotEqual(_versionI, _versionJ);
            Assert.NotEqual(_versionJ, _versionK);

#pragma warning disable xUnit2003 // Do not use equality check to test for null value
            Assert.NotEqual(null, _versionA);
#pragma warning restore xUnit2003 // Do not use equality check to test for null value
        }

        [Fact]
        public void SemanticVersion_GetHashCode_ReturnsHashCodeFollowingEqualsResults()
        {
            Assert.Equal(_versionA.GetHashCode(), _versionA.GetHashCode());
            Assert.NotEqual(_versionA.GetHashCode(), _versionB.GetHashCode());
            Assert.NotEqual(_versionA.GetHashCode(), _versionH.GetHashCode());

            Assert.NotEqual(_versionH.GetHashCode(), _versionA.GetHashCode());
            Assert.NotEqual(_versionH.GetHashCode(), _versionI.GetHashCode());

            Assert.Equal(_versionG.GetHashCode(), _versionG.GetHashCode());
            Assert.Equal(_versionG.GetHashCode(), _versionGBis.GetHashCode());
            Assert.Equal(_versionGBis.GetHashCode(), _versionGBis.GetHashCode());

            Assert.NotEqual(_versionI.GetHashCode(), _versionJ.GetHashCode());
            Assert.NotEqual(_versionJ.GetHashCode(), _versionK.GetHashCode());
        }

        [Fact]
        public void SemanticVersion_StrictComparisonOperators_ReturnsComparisonResults()
        {
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.False(_versionA > _versionA);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionA < _versionB);
            Assert.True(_versionA < _versionC);
            Assert.True(_versionA < _versionD);
            Assert.True(_versionA < _versionE);
            Assert.True(_versionA < _versionF);
            Assert.True(_versionA < _versionG);
            Assert.True(_versionA < _versionH);
            Assert.True(_versionA < _versionI);
            Assert.True(_versionA < _versionJ);
            Assert.True(_versionA < _versionK);
            Assert.True(_versionA < _versionL);

            Assert.True(_versionB > _versionA);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.False(_versionB > _versionB);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionB < _versionC);
            Assert.True(_versionB < _versionD);
            Assert.True(_versionB < _versionE);
            Assert.True(_versionB < _versionF);
            Assert.True(_versionB < _versionG);
            Assert.True(_versionB < _versionH);
            Assert.True(_versionB < _versionI);
            Assert.True(_versionB < _versionJ);
            Assert.True(_versionB < _versionK);
            Assert.True(_versionB < _versionL);

            Assert.True(_versionC > _versionA);
            Assert.True(_versionC > _versionB);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.False(_versionC > _versionC);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionC < _versionD);
            Assert.True(_versionC < _versionE);
            Assert.True(_versionC < _versionF);
            Assert.True(_versionC < _versionG);
            Assert.True(_versionC < _versionH);
            Assert.True(_versionC < _versionI);
            Assert.True(_versionC < _versionJ);
            Assert.True(_versionC < _versionK);
            Assert.True(_versionC < _versionL);

            Assert.True(_versionD > _versionA);
            Assert.True(_versionD > _versionB);
            Assert.True(_versionD > _versionC);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.False(_versionD > _versionD);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionD < _versionE);
            Assert.True(_versionD < _versionF);
            Assert.True(_versionD < _versionG);
            Assert.True(_versionD < _versionH);
            Assert.True(_versionD < _versionI);
            Assert.True(_versionD < _versionJ);
            Assert.True(_versionD < _versionK);
            Assert.True(_versionD < _versionL);

            Assert.True(_versionE > _versionA);
            Assert.True(_versionE > _versionB);
            Assert.True(_versionE > _versionC);
            Assert.True(_versionE > _versionD);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.False(_versionE > _versionE);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionE < _versionF);
            Assert.True(_versionE < _versionG);
            Assert.True(_versionE < _versionH);
            Assert.True(_versionE < _versionI);
            Assert.True(_versionE < _versionJ);
            Assert.True(_versionE < _versionK);
            Assert.True(_versionE < _versionL);

            Assert.True(_versionF > _versionA);
            Assert.True(_versionF > _versionB);
            Assert.True(_versionF > _versionC);
            Assert.True(_versionF > _versionD);
            Assert.True(_versionF > _versionE);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.False(_versionF > _versionF);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionF < _versionG);
            Assert.True(_versionF < _versionH);
            Assert.True(_versionF < _versionI);
            Assert.True(_versionF < _versionJ);
            Assert.True(_versionF < _versionK);
            Assert.True(_versionF < _versionL);

            Assert.True(_versionG > _versionA);
            Assert.True(_versionG > _versionB);
            Assert.True(_versionG > _versionC);
            Assert.True(_versionG > _versionD);
            Assert.True(_versionG > _versionE);
            Assert.True(_versionG > _versionF);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.False(_versionG > _versionG);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionG < _versionH);
            Assert.True(_versionG < _versionI);
            Assert.True(_versionG < _versionJ);
            Assert.True(_versionG < _versionK);
            Assert.True(_versionG < _versionL);

            Assert.True(_versionH > _versionA);
            Assert.True(_versionH > _versionB);
            Assert.True(_versionH > _versionC);
            Assert.True(_versionH > _versionD);
            Assert.True(_versionH > _versionE);
            Assert.True(_versionH > _versionF);
            Assert.True(_versionH > _versionG);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.False(_versionH > _versionH);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionH < _versionI);
            Assert.True(_versionH < _versionJ);
            Assert.True(_versionH < _versionK);
            Assert.True(_versionH < _versionL);

            Assert.True(_versionI > _versionA);
            Assert.True(_versionI > _versionB);
            Assert.True(_versionI > _versionC);
            Assert.True(_versionI > _versionD);
            Assert.True(_versionI > _versionE);
            Assert.True(_versionI > _versionF);
            Assert.True(_versionI > _versionG);
            Assert.True(_versionI > _versionH);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.False(_versionI > _versionI);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionI < _versionJ);
            Assert.True(_versionI < _versionK);
            Assert.True(_versionI < _versionL);

            Assert.True(_versionJ > _versionA);
            Assert.True(_versionJ > _versionB);
            Assert.True(_versionJ > _versionC);
            Assert.True(_versionJ > _versionD);
            Assert.True(_versionJ > _versionE);
            Assert.True(_versionJ > _versionF);
            Assert.True(_versionJ > _versionG);
            Assert.True(_versionJ > _versionH);
            Assert.True(_versionJ > _versionI);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.False(_versionJ > _versionJ);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionJ < _versionK);
            Assert.True(_versionJ < _versionL);

            Assert.True(_versionK > _versionA);
            Assert.True(_versionK > _versionB);
            Assert.True(_versionK > _versionC);
            Assert.True(_versionK > _versionD);
            Assert.True(_versionK > _versionE);
            Assert.True(_versionK > _versionF);
            Assert.True(_versionK > _versionG);
            Assert.True(_versionK > _versionH);
            Assert.True(_versionK > _versionI);
            Assert.True(_versionK > _versionJ);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.False(_versionK > _versionK);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionK < _versionL);

            Assert.True(_versionL > _versionA);
            Assert.True(_versionL > _versionB);
            Assert.True(_versionL > _versionC);
            Assert.True(_versionL > _versionD);
            Assert.True(_versionL > _versionE);
            Assert.True(_versionL > _versionF);
            Assert.True(_versionL > _versionG);
            Assert.True(_versionL > _versionH);
            Assert.True(_versionL > _versionI);
            Assert.True(_versionL > _versionJ);
            Assert.True(_versionL > _versionK);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.False(_versionL > _versionL);
#pragma warning restore CS1718 // Comparison made to same variable

            Assert.False(_versionG > _versionGBis);
            Assert.False(_versionG < _versionGBis);

            Assert.False(_versionA > null);
            Assert.False(_versionA < null);

            Assert.False((SemanticVersion)null > null);
            Assert.False((SemanticVersion)null < null);
        }

        [Fact]
        public void SemanticVersion_NotStrictComparisonOperators_ReturnsComparisonResults()
        {
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.True(_versionA >= _versionA);
            Assert.True(_versionA <= _versionA);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionA <= _versionB);
            Assert.True(_versionA <= _versionC);
            Assert.True(_versionA <= _versionD);
            Assert.True(_versionA <= _versionE);
            Assert.True(_versionA <= _versionF);
            Assert.True(_versionA <= _versionG);
            Assert.True(_versionA <= _versionH);
            Assert.True(_versionA <= _versionI);
            Assert.True(_versionA <= _versionJ);
            Assert.True(_versionA <= _versionK);
            Assert.True(_versionA <= _versionL);

            Assert.True(_versionB >= _versionA);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.True(_versionB >= _versionB);
            Assert.True(_versionB <= _versionB);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionB <= _versionC);
            Assert.True(_versionB <= _versionD);
            Assert.True(_versionB <= _versionE);
            Assert.True(_versionB <= _versionF);
            Assert.True(_versionB <= _versionG);
            Assert.True(_versionB <= _versionH);
            Assert.True(_versionB <= _versionI);
            Assert.True(_versionB <= _versionJ);
            Assert.True(_versionB <= _versionK);
            Assert.True(_versionB <= _versionL);

            Assert.True(_versionC >= _versionA);
            Assert.True(_versionC >= _versionB);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.True(_versionC >= _versionC);
            Assert.True(_versionC <= _versionC);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionC <= _versionD);
            Assert.True(_versionC <= _versionE);
            Assert.True(_versionC <= _versionF);
            Assert.True(_versionC <= _versionG);
            Assert.True(_versionC <= _versionH);
            Assert.True(_versionC <= _versionI);
            Assert.True(_versionC <= _versionJ);
            Assert.True(_versionC <= _versionK);
            Assert.True(_versionC <= _versionL);

            Assert.True(_versionD >= _versionA);
            Assert.True(_versionD >= _versionB);
            Assert.True(_versionD >= _versionC);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.True(_versionD >= _versionD);
            Assert.True(_versionD <= _versionD);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionD <= _versionE);
            Assert.True(_versionD <= _versionF);
            Assert.True(_versionD <= _versionG);
            Assert.True(_versionD <= _versionH);
            Assert.True(_versionD <= _versionI);
            Assert.True(_versionD <= _versionJ);
            Assert.True(_versionD <= _versionK);
            Assert.True(_versionD <= _versionL);

            Assert.True(_versionE >= _versionA);
            Assert.True(_versionE >= _versionB);
            Assert.True(_versionE >= _versionC);
            Assert.True(_versionE >= _versionD);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.True(_versionE >= _versionE);
            Assert.True(_versionE <= _versionE);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionE <= _versionF);
            Assert.True(_versionE <= _versionG);
            Assert.True(_versionE <= _versionH);
            Assert.True(_versionE <= _versionI);
            Assert.True(_versionE <= _versionJ);
            Assert.True(_versionE <= _versionK);
            Assert.True(_versionE <= _versionL);

            Assert.True(_versionF >= _versionA);
            Assert.True(_versionF >= _versionB);
            Assert.True(_versionF >= _versionC);
            Assert.True(_versionF >= _versionD);
            Assert.True(_versionF >= _versionE);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.True(_versionF >= _versionF);
            Assert.True(_versionF <= _versionF);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionF <= _versionG);
            Assert.True(_versionF <= _versionH);
            Assert.True(_versionF <= _versionI);
            Assert.True(_versionF <= _versionJ);
            Assert.True(_versionF <= _versionK);
            Assert.True(_versionF <= _versionL);

            Assert.True(_versionG >= _versionA);
            Assert.True(_versionG >= _versionB);
            Assert.True(_versionG >= _versionC);
            Assert.True(_versionG >= _versionD);
            Assert.True(_versionG >= _versionE);
            Assert.True(_versionG >= _versionF);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.True(_versionG >= _versionG);
            Assert.True(_versionG <= _versionG);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionG <= _versionH);
            Assert.True(_versionG <= _versionI);
            Assert.True(_versionG <= _versionJ);
            Assert.True(_versionG <= _versionK);
            Assert.True(_versionG <= _versionL);

            Assert.True(_versionH >= _versionA);
            Assert.True(_versionH >= _versionB);
            Assert.True(_versionH >= _versionC);
            Assert.True(_versionH >= _versionD);
            Assert.True(_versionH >= _versionE);
            Assert.True(_versionH >= _versionF);
            Assert.True(_versionH >= _versionG);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.True(_versionH >= _versionH);
            Assert.True(_versionH <= _versionH);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionH <= _versionI);
            Assert.True(_versionH <= _versionJ);
            Assert.True(_versionH <= _versionK);
            Assert.True(_versionH <= _versionL);

            Assert.True(_versionI >= _versionA);
            Assert.True(_versionI >= _versionB);
            Assert.True(_versionI >= _versionC);
            Assert.True(_versionI >= _versionD);
            Assert.True(_versionI >= _versionE);
            Assert.True(_versionI >= _versionF);
            Assert.True(_versionI >= _versionG);
            Assert.True(_versionI >= _versionH);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.True(_versionI >= _versionI);
            Assert.True(_versionI <= _versionI);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionI <= _versionJ);
            Assert.True(_versionI <= _versionK);
            Assert.True(_versionI <= _versionL);

            Assert.True(_versionJ >= _versionA);
            Assert.True(_versionJ >= _versionB);
            Assert.True(_versionJ >= _versionC);
            Assert.True(_versionJ >= _versionD);
            Assert.True(_versionJ >= _versionE);
            Assert.True(_versionJ >= _versionF);
            Assert.True(_versionJ >= _versionG);
            Assert.True(_versionJ >= _versionH);
            Assert.True(_versionJ >= _versionI);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.True(_versionJ >= _versionJ);
            Assert.True(_versionJ <= _versionJ);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionJ <= _versionK);
            Assert.True(_versionJ <= _versionL);

            Assert.True(_versionK >= _versionA);
            Assert.True(_versionK >= _versionB);
            Assert.True(_versionK >= _versionC);
            Assert.True(_versionK >= _versionD);
            Assert.True(_versionK >= _versionE);
            Assert.True(_versionK >= _versionF);
            Assert.True(_versionK >= _versionG);
            Assert.True(_versionK >= _versionH);
            Assert.True(_versionK >= _versionI);
            Assert.True(_versionK >= _versionJ);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.True(_versionK >= _versionK);
            Assert.True(_versionK <= _versionK);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionK <= _versionL);

            Assert.True(_versionL >= _versionA);
            Assert.True(_versionL >= _versionB);
            Assert.True(_versionL >= _versionC);
            Assert.True(_versionL >= _versionD);
            Assert.True(_versionL >= _versionE);
            Assert.True(_versionL >= _versionF);
            Assert.True(_versionL >= _versionG);
            Assert.True(_versionL >= _versionH);
            Assert.True(_versionL >= _versionI);
            Assert.True(_versionL >= _versionJ);
            Assert.True(_versionL >= _versionK);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.True(_versionL >= _versionL);
            Assert.True(_versionL <= _versionL);
#pragma warning restore CS1718 // Comparison made to same variable

            Assert.True(_versionG >= _versionGBis);
            Assert.True(_versionG <= _versionGBis);

            Assert.False(_versionA >= null);
            Assert.False(_versionA <= null);

            Assert.True((SemanticVersion)null >= null);
            Assert.True((SemanticVersion)null <= null);
        }

        [Fact]
        public void SemanticVersion_EqualityOperators_ReturnsEqualityResults()
        {
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.False(_versionA != _versionA);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionA != _versionB);
            Assert.True(_versionA != _versionC);
            Assert.True(_versionA != _versionD);
            Assert.True(_versionA != _versionE);
            Assert.True(_versionA != _versionF);
            Assert.True(_versionA != _versionG);
            Assert.True(_versionA != _versionH);
            Assert.True(_versionA != _versionI);
            Assert.True(_versionA != _versionJ);
            Assert.True(_versionA != _versionK);
            Assert.True(_versionA != _versionL);

            Assert.True(_versionB != _versionA);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.False(_versionB != _versionB);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionB != _versionC);
            Assert.True(_versionB != _versionD);
            Assert.True(_versionB != _versionE);
            Assert.True(_versionB != _versionF);
            Assert.True(_versionB != _versionG);
            Assert.True(_versionB != _versionH);
            Assert.True(_versionB != _versionI);
            Assert.True(_versionB != _versionJ);
            Assert.True(_versionB != _versionK);
            Assert.True(_versionB != _versionL);

            Assert.True(_versionC != _versionA);
            Assert.True(_versionC != _versionB);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.False(_versionC != _versionC);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionC != _versionD);
            Assert.True(_versionC != _versionE);
            Assert.True(_versionC != _versionF);
            Assert.True(_versionC != _versionG);
            Assert.True(_versionC != _versionH);
            Assert.True(_versionC != _versionI);
            Assert.True(_versionC != _versionJ);
            Assert.True(_versionC != _versionK);
            Assert.True(_versionC != _versionL);

            Assert.True(_versionD != _versionA);
            Assert.True(_versionD != _versionB);
            Assert.True(_versionD != _versionC);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.False(_versionD != _versionD);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionD != _versionE);
            Assert.True(_versionD != _versionF);
            Assert.True(_versionD != _versionG);
            Assert.True(_versionD != _versionH);
            Assert.True(_versionD != _versionI);
            Assert.True(_versionD != _versionJ);
            Assert.True(_versionD != _versionK);
            Assert.True(_versionD != _versionL);

            Assert.True(_versionE != _versionA);
            Assert.True(_versionE != _versionB);
            Assert.True(_versionE != _versionC);
            Assert.True(_versionE != _versionD);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.False(_versionE != _versionE);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionE != _versionF);
            Assert.True(_versionE != _versionG);
            Assert.True(_versionE != _versionH);
            Assert.True(_versionE != _versionI);
            Assert.True(_versionE != _versionJ);
            Assert.True(_versionE != _versionK);
            Assert.True(_versionE != _versionL);

            Assert.True(_versionF != _versionA);
            Assert.True(_versionF != _versionB);
            Assert.True(_versionF != _versionC);
            Assert.True(_versionF != _versionD);
            Assert.True(_versionF != _versionE);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.False(_versionF != _versionF);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionF != _versionG);
            Assert.True(_versionF != _versionH);
            Assert.True(_versionF != _versionI);
            Assert.True(_versionF != _versionJ);
            Assert.True(_versionF != _versionK);
            Assert.True(_versionF != _versionL);

            Assert.True(_versionG != _versionA);
            Assert.True(_versionG != _versionB);
            Assert.True(_versionG != _versionC);
            Assert.True(_versionG != _versionD);
            Assert.True(_versionG != _versionE);
            Assert.True(_versionG != _versionF);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.False(_versionG != _versionG);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionG != _versionH);
            Assert.True(_versionG != _versionI);
            Assert.True(_versionG != _versionJ);
            Assert.True(_versionG != _versionK);
            Assert.True(_versionG != _versionL);

            Assert.True(_versionH != _versionA);
            Assert.True(_versionH != _versionB);
            Assert.True(_versionH != _versionC);
            Assert.True(_versionH != _versionD);
            Assert.True(_versionH != _versionE);
            Assert.True(_versionH != _versionF);
            Assert.True(_versionH != _versionG);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.False(_versionH != _versionH);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionH != _versionI);
            Assert.True(_versionH != _versionJ);
            Assert.True(_versionH != _versionK);
            Assert.True(_versionH != _versionL);

            Assert.True(_versionI != _versionA);
            Assert.True(_versionI != _versionB);
            Assert.True(_versionI != _versionC);
            Assert.True(_versionI != _versionD);
            Assert.True(_versionI != _versionE);
            Assert.True(_versionI != _versionF);
            Assert.True(_versionI != _versionG);
            Assert.True(_versionI != _versionH);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.False(_versionI != _versionI);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionI != _versionJ);
            Assert.True(_versionI != _versionK);
            Assert.True(_versionI != _versionL);

            Assert.True(_versionJ != _versionA);
            Assert.True(_versionJ != _versionB);
            Assert.True(_versionJ != _versionC);
            Assert.True(_versionJ != _versionD);
            Assert.True(_versionJ != _versionE);
            Assert.True(_versionJ != _versionF);
            Assert.True(_versionJ != _versionG);
            Assert.True(_versionJ != _versionH);
            Assert.True(_versionJ != _versionI);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.False(_versionJ != _versionJ);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionJ != _versionK);
            Assert.True(_versionJ != _versionL);

            Assert.True(_versionK != _versionA);
            Assert.True(_versionK != _versionB);
            Assert.True(_versionK != _versionC);
            Assert.True(_versionK != _versionD);
            Assert.True(_versionK != _versionE);
            Assert.True(_versionK != _versionF);
            Assert.True(_versionK != _versionG);
            Assert.True(_versionK != _versionH);
            Assert.True(_versionK != _versionI);
            Assert.True(_versionK != _versionJ);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.False(_versionK != _versionK);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.True(_versionK != _versionL);

            Assert.True(_versionL != _versionA);
            Assert.True(_versionL != _versionB);
            Assert.True(_versionL != _versionC);
            Assert.True(_versionL != _versionD);
            Assert.True(_versionL != _versionE);
            Assert.True(_versionL != _versionF);
            Assert.True(_versionL != _versionG);
            Assert.True(_versionL != _versionH);
            Assert.True(_versionL != _versionI);
            Assert.True(_versionL != _versionJ);
            Assert.True(_versionL != _versionK);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.False(_versionL != _versionL);
#pragma warning restore CS1718 // Comparison made to same variable

            Assert.False(_versionG != _versionGBis);
            Assert.True(_versionA != null);

#pragma warning disable CS1718 // Comparison made to same variable
            Assert.True(_versionA == _versionA);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.False(_versionA == _versionB);
            Assert.False(_versionA == _versionC);
            Assert.False(_versionA == _versionD);
            Assert.False(_versionA == _versionE);
            Assert.False(_versionA == _versionF);
            Assert.False(_versionA == _versionG);
            Assert.False(_versionA == _versionH);
            Assert.False(_versionA == _versionI);
            Assert.False(_versionA == _versionJ);
            Assert.False(_versionA == _versionK);
            Assert.False(_versionA == _versionL);

            Assert.False(_versionB == _versionA);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.True(_versionB == _versionB);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.False(_versionB == _versionC);
            Assert.False(_versionB == _versionD);
            Assert.False(_versionB == _versionE);
            Assert.False(_versionB == _versionF);
            Assert.False(_versionB == _versionG);
            Assert.False(_versionB == _versionH);
            Assert.False(_versionB == _versionI);
            Assert.False(_versionB == _versionJ);
            Assert.False(_versionB == _versionK);
            Assert.False(_versionB == _versionL);

            Assert.False(_versionC == _versionA);
            Assert.False(_versionC == _versionB);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.True(_versionC == _versionC);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.False(_versionC == _versionD);
            Assert.False(_versionC == _versionE);
            Assert.False(_versionC == _versionF);
            Assert.False(_versionC == _versionG);
            Assert.False(_versionC == _versionH);
            Assert.False(_versionC == _versionI);
            Assert.False(_versionC == _versionJ);
            Assert.False(_versionC == _versionK);
            Assert.False(_versionC == _versionL);

            Assert.False(_versionD == _versionA);
            Assert.False(_versionD == _versionB);
            Assert.False(_versionD == _versionC);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.True(_versionD == _versionD);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.False(_versionD == _versionE);
            Assert.False(_versionD == _versionF);
            Assert.False(_versionD == _versionG);
            Assert.False(_versionD == _versionH);
            Assert.False(_versionD == _versionI);
            Assert.False(_versionD == _versionJ);
            Assert.False(_versionD == _versionK);
            Assert.False(_versionD == _versionL);

            Assert.False(_versionE == _versionA);
            Assert.False(_versionE == _versionB);
            Assert.False(_versionE == _versionC);
            Assert.False(_versionE == _versionD);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.True(_versionE == _versionE);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.False(_versionE == _versionF);
            Assert.False(_versionE == _versionG);
            Assert.False(_versionE == _versionH);
            Assert.False(_versionE == _versionI);
            Assert.False(_versionE == _versionJ);
            Assert.False(_versionE == _versionK);
            Assert.False(_versionE == _versionL);

            Assert.False(_versionF == _versionA);
            Assert.False(_versionF == _versionB);
            Assert.False(_versionF == _versionC);
            Assert.False(_versionF == _versionD);
            Assert.False(_versionF == _versionE);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.True(_versionF == _versionF);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.False(_versionF == _versionG);
            Assert.False(_versionF == _versionH);
            Assert.False(_versionF == _versionI);
            Assert.False(_versionF == _versionJ);
            Assert.False(_versionF == _versionK);
            Assert.False(_versionF == _versionL);

            Assert.False(_versionG == _versionA);
            Assert.False(_versionG == _versionB);
            Assert.False(_versionG == _versionC);
            Assert.False(_versionG == _versionD);
            Assert.False(_versionG == _versionE);
            Assert.False(_versionG == _versionF);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.True(_versionG == _versionG);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.False(_versionG == _versionH);
            Assert.False(_versionG == _versionI);
            Assert.False(_versionG == _versionJ);
            Assert.False(_versionG == _versionK);
            Assert.False(_versionG == _versionL);

            Assert.False(_versionH == _versionA);
            Assert.False(_versionH == _versionB);
            Assert.False(_versionH == _versionC);
            Assert.False(_versionH == _versionD);
            Assert.False(_versionH == _versionE);
            Assert.False(_versionH == _versionF);
            Assert.False(_versionH == _versionG);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.True(_versionH == _versionH);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.False(_versionH == _versionI);
            Assert.False(_versionH == _versionJ);
            Assert.False(_versionH == _versionK);
            Assert.False(_versionH == _versionL);

            Assert.False(_versionI == _versionA);
            Assert.False(_versionI == _versionB);
            Assert.False(_versionI == _versionC);
            Assert.False(_versionI == _versionD);
            Assert.False(_versionI == _versionE);
            Assert.False(_versionI == _versionF);
            Assert.False(_versionI == _versionG);
            Assert.False(_versionI == _versionH);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.True(_versionI == _versionI);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.False(_versionI == _versionJ);
            Assert.False(_versionI == _versionK);
            Assert.False(_versionI == _versionL);

            Assert.False(_versionJ == _versionA);
            Assert.False(_versionJ == _versionB);
            Assert.False(_versionJ == _versionC);
            Assert.False(_versionJ == _versionD);
            Assert.False(_versionJ == _versionE);
            Assert.False(_versionJ == _versionF);
            Assert.False(_versionJ == _versionG);
            Assert.False(_versionJ == _versionH);
            Assert.False(_versionJ == _versionI);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.True(_versionJ == _versionJ);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.False(_versionJ == _versionK);
            Assert.False(_versionJ == _versionL);

            Assert.False(_versionK == _versionA);
            Assert.False(_versionK == _versionB);
            Assert.False(_versionK == _versionC);
            Assert.False(_versionK == _versionD);
            Assert.False(_versionK == _versionE);
            Assert.False(_versionK == _versionF);
            Assert.False(_versionK == _versionG);
            Assert.False(_versionK == _versionH);
            Assert.False(_versionK == _versionI);
            Assert.False(_versionK == _versionJ);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.True(_versionK == _versionK);
#pragma warning restore CS1718 // Comparison made to same variable
            Assert.False(_versionK == _versionL);

            Assert.False(_versionL == _versionA);
            Assert.False(_versionL == _versionB);
            Assert.False(_versionL == _versionC);
            Assert.False(_versionL == _versionD);
            Assert.False(_versionL == _versionE);
            Assert.False(_versionL == _versionF);
            Assert.False(_versionL == _versionG);
            Assert.False(_versionL == _versionH);
            Assert.False(_versionL == _versionI);
            Assert.False(_versionL == _versionJ);
            Assert.False(_versionL == _versionK);
#pragma warning disable CS1718 // Comparison made to same variable
            Assert.True(_versionL == _versionL);
#pragma warning restore CS1718 // Comparison made to same variable

            Assert.True(_versionG == _versionGBis);
            Assert.False(_versionA == null);
        }

        [Theory]
        [InlineData("")]
        [InlineData("1.2")]
        [InlineData("01.2.3")]
        [InlineData("1.02.3")]
        [InlineData("1.2.03")]
        [InlineData("2.4.12-foo-bar-béaz+qux.42.43-quux.12")]
        [InlineData("2.4.12-foo-bar-baz+qux.42.43-quéux.12")]
        public void SemanticVersion_Parse_WithInvalidString_ThrowsException(string str)
        {
            Assert.Throws<ArgumentException>(() => SemanticVersion.Parse(str));
        }

        [Fact]
        public void SemanticVersion_Parse_WithValidString_ReturnsCorrespondingSemanticVersion()
        {
            var semanticVersion1 = SemanticVersion.Parse("0.1.0");
            var semanticVersion2 = SemanticVersion.Parse("1.2.3");
            var semanticVersion3 = SemanticVersion.Parse("531.545614.45646163");
            var semanticVersion4 = SemanticVersion.Parse("2.4.12-foo-bar42-ba8z");
            var semanticVersion5 = SemanticVersion.Parse("2.4.12-foo-bar42-ba8z+qux.42.43-quux.12");
            var semanticVersion6 = SemanticVersion.Parse("2.4.12-foo.bar42-ba8z+qux.42.43-quux.12");
            var semanticVersion7 = SemanticVersion.Parse("2.4.12+qux.42.43-quux.12-foo-bar-baz");

            Assert.Equal(new SemanticVersion(0, 1, 0), semanticVersion1);
            Assert.Equal(new SemanticVersion(1, 2, 3), semanticVersion2);
            Assert.Equal(new SemanticVersion(531, 545614, 45646163), semanticVersion3);
            Assert.Equal(new SemanticVersion(2, 4, 12, "-foo-bar42-ba8z"), semanticVersion4);
            Assert.Equal(new SemanticVersion(2, 4, 12, "-foo-bar42-ba8z", "+qux.42.43-quux.12"), semanticVersion5);
            Assert.Equal(new SemanticVersion(2, 4, 12, "-foo.bar42-ba8z", "+qux.42.43-quux.12"), semanticVersion6);
            Assert.Equal(new SemanticVersion(2, 4, 12, null, "+qux.42.43-quux.12-foo-bar-baz"), semanticVersion7);
        }

        [Theory]
        [InlineData("")]
        [InlineData("1.2")]
        [InlineData("01.2.3")]
        [InlineData("1.02.3")]
        [InlineData("1.2.03")]
        [InlineData("2.4.12-foo-bar-béaz+qux.42.43-quux.12")]
        [InlineData("2.4.12-foo-bar-baz+qux.42.43-quéux.12")]
        public void SemanticVersion_TryParse_WithInvalidString_ReturnsFalse(string str)
        {
            Assert.False(SemanticVersion.TryParse(str, out SemanticVersion _));
        }

        [Fact]
        public void SemanticVersion_TryParse_WithValidString_ReturnsTrueAndOutputsCorrespondingSemanticVersion()
        {
            bool result1 = SemanticVersion.TryParse("0.1.0", out SemanticVersion semanticVersion1);
            bool result2 = SemanticVersion.TryParse("1.2.3", out SemanticVersion semanticVersion2);
            bool result3 = SemanticVersion.TryParse("531.545614.45646163", out SemanticVersion semanticVersion3);
            bool result4 = SemanticVersion.TryParse("2.4.12-foo-bar42-ba8z", out SemanticVersion semanticVersion4);
            bool result5 = SemanticVersion.TryParse("2.4.12-foo-bar42-ba8z+qux.42.43-quux.12", out SemanticVersion semanticVersion5);
            bool result6 = SemanticVersion.TryParse("2.4.12-foo.bar42-ba8z+qux.42.43-quux.12", out SemanticVersion semanticVersion6);
            bool result7 = SemanticVersion.TryParse("2.4.12+qux.42.43-quux.12-foo-bar-baz", out SemanticVersion semanticVersion7);

            Assert.True(result1);
            Assert.True(result2);
            Assert.True(result3);
            Assert.True(result4);
            Assert.True(result5);
            Assert.True(result6);
            Assert.True(result7);

            Assert.Equal(new SemanticVersion(0, 1, 0), semanticVersion1);
            Assert.Equal(new SemanticVersion(1, 2, 3), semanticVersion2);
            Assert.Equal(new SemanticVersion(531, 545614, 45646163), semanticVersion3);
            Assert.Equal(new SemanticVersion(2, 4, 12, "-foo-bar42-ba8z"), semanticVersion4);
            Assert.Equal(new SemanticVersion(2, 4, 12, "-foo-bar42-ba8z", "+qux.42.43-quux.12"), semanticVersion5);
            Assert.Equal(new SemanticVersion(2, 4, 12, "-foo.bar42-ba8z", "+qux.42.43-quux.12"), semanticVersion6);
            Assert.Equal(new SemanticVersion(2, 4, 12, null, "+qux.42.43-quux.12-foo-bar-baz"), semanticVersion7);
        }

        [Fact]
        public void SemanticVersion_CanBeSerialized()
        {
            string json = JsonConvert.SerializeObject(_versionGBis);
            Assert.Equal("{\"Build\":\"+foo.bar.baz\",\"Major\":42,\"Minor\":233,\"Patch\":17,\"Prerelease\":\"-rc.1\"}", json);
        }
    }
}
