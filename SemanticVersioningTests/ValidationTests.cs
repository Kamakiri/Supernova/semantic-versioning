﻿using Supernova.SemanticVersioning;
using Xunit;

namespace SemanticVersioningTests
{
    public class ValidationTests
    {
        [Theory]
        [InlineData("0.1.0")]
        [InlineData("1.2.3")]
        [InlineData("531.545614.45646163")]
        [InlineData("2.4.12-foo-bar42-ba8z")]
        [InlineData("2.4.12-foo-bar42-ba8z+qux.42.43-quux.12")]
        [InlineData("2.4.12-foo.bar42-ba8z+qux.42.43-quux.12")]
        [InlineData("2.4.12+qux.42.43-quux.12-foo-bar-baz")]
        public void Validation_SemanticVersionRegex_MatchesValidStrings(string str)
        {
            Assert.Matches(Validation.SemanticVersionRegex, str);
        }

        [Theory]
        [InlineData("")]
        [InlineData("1.2")]
        [InlineData("01.2.3")]
        [InlineData("1.02.3")]
        [InlineData("1.2.03")]
        [InlineData("2.4.12-foo-bar-béaz+qux.42.43-quux.12")]
        [InlineData("2.4.12-foo-bar-baz+qux.42.43-quéux.12")]
        public void Validation_SemanticVersionRegex_DoesNotMatchInvalidStrings(string str)
        {
            Assert.DoesNotMatch(Validation.SemanticVersionRegex, str);
        }

        [Theory]
        [InlineData("-foo")]
        [InlineData("-42")]
        [InlineData("-foo-bar42-ba8z")]
        public void Validation_PrereleaseRegex_MatchesValidStrings(string str)
        {
            Assert.Matches($"^{Validation.PrereleaseRegex}$", str);
        }

        [Theory]
        [InlineData("")]
        [InlineData("-")]
        [InlineData("+foo")]
        [InlineData("-foo+")]
        [InlineData("-fooé")]
        [InlineData("-foo.bar_baz")]
        public void Validation_PrereleaseRegex_DoesNotMatchInvalidStrings(string str)
        {
            Assert.DoesNotMatch($"^{Validation.PrereleaseRegex}$", str);
        }

        [Theory]
        [InlineData("+foo")]
        [InlineData("+foo-")]
        [InlineData("+42")]
        [InlineData("+foo-bar42-ba8z")]
        [InlineData("+foo.bar42-ba8z")]
        public void Validation_BuildRegex_MatchesValidStrings(string str)
        {
            Assert.Matches($"^{Validation.BuildRegex}$", str);
        }

        [Theory]
        [InlineData("")]
        [InlineData("-")]
        [InlineData("-foo")]
        [InlineData("+fooé")]
        [InlineData("+foo.bar_baz")]
        public void Validation_BuildRegex_DoesNotMatchInvalidStrings(string str)
        {
            Assert.DoesNotMatch($"^{Validation.BuildRegex}$", str);
        }
    }
}
