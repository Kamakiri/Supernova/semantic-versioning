﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using Supernova.Foundation.Core;

namespace Supernova.SemanticVersioning
{
    /// <summary>
    /// Represents a version, following Semantic Version Specification 2.0.0 (https://semver.org/#semantic-versioning-200).
    /// </summary>
    public class SemanticVersion : IComparable<SemanticVersion>, IEquatable<SemanticVersion>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SemanticVersion"/> class.
        /// </summary>
        /// <param name="major">The major version.</param>
        /// <param name="minor">The minor version.</param>
        /// <param name="patch">The patch version.</param>
        /// <param name="prerelease">An optional pre-release version.</param>
        /// <param name="build">Optional build metadata.</param>
        public SemanticVersion(int major, int minor, int patch, string prerelease = null, string build = null)
        {
            Guard.Positive(major, nameof(major));
            Guard.Positive(minor, nameof(minor));
            Guard.Positive(patch, nameof(patch));
            Guard.Require(prerelease is null || Validation.PrereleaseRegex.IsMatch(prerelease), "Prerelease must follow specification format.");
            Guard.Require(build is null || Validation.BuildRegex.IsMatch(build), "Build must follow specification format.");

            Major = major;
            Minor = minor;
            Patch = patch;
            Prerelease = prerelease;
            Build = build;
        }

        /// <summary>
        /// Gets the build metadata.
        /// </summary>
        public string Build { get; }

        /// <summary>
        /// Gets a new instance of initial version (0.1.0).
        /// </summary>
        public static SemanticVersion Initial => new SemanticVersion(0, 1, 0);

        /// <summary>
        /// Gets the major version.
        /// </summary>
        public int Major { get; }

        /// <summary>
        /// Gets the minor version.
        /// </summary>
        public int Minor { get; }

        /// <summary>
        /// The patch version.
        /// </summary>
        public int Patch { get; }

        /// <summary>
        /// The pre-release version.
        /// </summary>
        public string Prerelease { get; }

        /// <summary>
        /// Checks if two semantic versions are not equal.
        /// </summary>
        /// <param name="a">The first semantic version.</param>
        /// <param name="b">The second semantic version.</param>
        /// <returns><c>true</c> if <paramref name="a"/> and <paramref name="b"/> are not equal, <c>false</c> otherwise.</returns>
        public static bool operator !=(SemanticVersion a, SemanticVersion b) => !(a == b);

        /// <summary>
        /// Checks if a semantic version is strictly lower than another.
        /// </summary>
        /// <param name="a">The first semantic version.</param>
        /// <param name="b">The second semantic version.</param>
        /// <returns><c>true</c> if <paramref name="a"/> is strictly lower than <paramref name="b"/>, <c>false</c> otherwise.</returns>
        public static bool operator <(SemanticVersion a, SemanticVersion b)
        {
            if (a is null || b is null) return false;
            return a.CompareTo(b) < 0;
        }

        /// <summary>
        /// Checks if a semantic version is lower than another.
        /// </summary>
        /// <param name="a">The first semantic version.</param>
        /// <param name="b">The second semantic version.</param>
        /// <returns><c>true</c> if <paramref name="a"/> is lower than <paramref name="b"/>, <c>false</c> otherwise.</returns>
        public static bool operator <=(SemanticVersion a, SemanticVersion b)
        {
            if (a is null && b is null) return true;
            if (a is null || b is null) return false;
            return a.CompareTo(b) <= 0;
        }

        /// <summary>
        /// Checks if two semantic versions are equal.
        /// </summary>
        /// <param name="a">The first semantic version.</param>
        /// <param name="b">The second semantic version.</param>
        /// <returns><c>true</c> if <paramref name="a"/> and <paramref name="b"/> are equal, <c>false</c> otherwise.</returns>
        public static bool operator ==(SemanticVersion a, SemanticVersion b)
        {
            if (a is null && b is null) return true;
            if (a is null || b is null) return false;
            return a.Equals(b);
        }

        /// <summary>
        /// Checks if a semantic version is strictly greater than another.
        /// </summary>
        /// <param name="a">The first semantic version.</param>
        /// <param name="b">The second semantic version.</param>
        /// <returns><c>true</c> if <paramref name="a"/> is strictly greater than <paramref name="b"/>, <c>false</c> otherwise.</returns>
        public static bool operator >(SemanticVersion a, SemanticVersion b)
        {
            if (a is null || b is null) return false;
            return a.CompareTo(b) > 0;
        }

        /// <summary>
        /// Checks if a semantic version is greater than another.
        /// </summary>
        /// <param name="a">The first semantic version.</param>
        /// <param name="b">The second semantic version.</param>
        /// <returns><c>true</c> if <paramref name="a"/> is greater than <paramref name="b"/>, <c>false</c> otherwise.</returns>
        public static bool operator >=(SemanticVersion a, SemanticVersion b)
        {
            if (a is null && b is null) return true;
            if (a is null || b is null) return false;
            return a.CompareTo(b) >= 0;
        }

        /// <summary>
        /// Parses a string to an instance of <see cref="SemanticVersion"/>.
        /// </summary>
        /// <param name="input">The string.</param>
        /// <returns>The semantic version.</returns>
        public static SemanticVersion Parse(string input)
        {
            Guard.NotNullOrWhitespace(input, nameof(input));

            Match match = Validation.SemanticVersionRegex.Match(input);

            if (!match.Success) throw new ArgumentException("Invalid version string.", nameof(input));

            int major = int.Parse(match.Groups["major"].Value);
            int minor = int.Parse(match.Groups["minor"].Value);
            int patch = int.Parse(match.Groups["patch"].Value);
            string prerelease = match.Groups["prerelease"].Success ? match.Groups["prerelease"].Value : null;
            string build = match.Groups["build"].Success ? match.Groups["build"].Value : null;

            return new SemanticVersion(major, minor, patch, prerelease, build);
        }

        /// <summary>
        /// Tries to parse a string to an instance of <see cref="SemanticVersion"/>.
        /// </summary>
        /// <param name="input">The string.</param>
        /// <param name="semanticVersion">The semantic version on success, <c>null</c> on failure.</param>
        /// <returns><c>true</c> if the parsing succeded, <c>false</c> otherwise.</returns>
        public static bool TryParse(string input, out SemanticVersion semanticVersion)
        {
            try
            {
                semanticVersion = Parse(input);
                return true;
            }
            catch (Exception)
            {
                semanticVersion = null;
                return false;
            }
        }

        /// <summary>
        /// Compares the current instance to another <see cref="SemanticVersion"/> instance.
        /// Follows precedence specification (https://semver.org/#spec-item-11).
        /// </summary>
        /// <param name="other"></param>
        /// <returns><c>1</c> if <c>this</c> follows <paramref name="other"/>, <c>-1</c> if if precedes it, <c>0</c> if they are equal.</returns>
        public int CompareTo(SemanticVersion other)
        {
            if (other is null) return 1;
            if (Equals(other)) return 0;

            if (Major > other.Major) return 1;
            if (Major < other.Major) return -1;

            if (Minor > other.Minor) return 1;
            if (Minor < other.Minor) return -1;

            if (Patch > other.Patch) return 1;
            if (Patch < other.Patch) return -1;

            if (Prerelease is null) return 1;
            if (other.Prerelease is null) return -1;

            string[] identifiers = Prerelease.Split('.');
            string[] otherIdentifiers = other.Prerelease.Split('.');

            for (int i = 0; i < identifiers.Length; i++)
            {
                if (i >= otherIdentifiers.Length) return 1;

                string id = identifiers[i];
                string otherId = otherIdentifiers[i];

                if (id == otherId) continue;

                bool isDigits = id.All(c => c >= '0' && c <= '9');
                bool otherIsDigits = otherId.All(c => c >= '0' && c <= '9');

                if (!isDigits && otherIsDigits) return 1;
                if (isDigits && !otherIsDigits) return -1;

                if (isDigits) return int.Parse(id).CompareTo(int.Parse(otherId));

                return id.CompareTo(otherId);
            }

            if (identifiers.Length < otherIdentifiers.Length) return -1;
            return 0;
        }

        /// <summary>
        /// Determines if a provided <see cref="SemanticVersion"/> is equal to the current instance.
        /// Follows precedence specification (https://semver.org/#spec-item-11).
        /// </summary>
        /// <param name="other">The other instance.</param>
        /// <returns><c>true</c> if the versions are equal, <c>false</c> otherwise.</returns>
        public bool Equals(SemanticVersion other)
            => ReferenceEquals(other, this)
            || (!(other is null)
            && other.Major == Major
            && other.Minor == Minor
            && other.Patch == Patch
            && other.Prerelease == Prerelease);

        /// <summary>
        /// Determines if a provided object is equal to the current instance.
        /// Follows precedence specification (https://semver.org/#spec-item-11).
        /// </summary>
        /// <param name="obj">The other instance.</param>
        /// <returns><c>true</c> if the versions are equal, <c>false</c> otherwise.</returns>
        public override bool Equals(object obj) => Equals(obj as SemanticVersion);

        /// <summary>
        /// Computes the hash code of the current instance.
        /// Follows the <see cref="Equals(SemanticVersion)"/> method.
        /// </summary>
        /// <returns>The hash code.</returns>
        public override int GetHashCode()
        {
            const int PRIME = 16777619;
            unchecked
            {
                int hash = (int)2166136261;
                hash = (hash * PRIME) ^ Major.GetHashCode();
                hash = (hash * PRIME) ^ Minor.GetHashCode();
                hash = (hash * PRIME) ^ Patch.GetHashCode();
                hash = (hash * PRIME) ^ (Prerelease?.GetHashCode() ?? 0);
                return hash;
            }
        }

        /// <summary>
        /// Creates a new instance of <see cref="SemanticVersion"/> from the current instance, where the major version has been incremented by 1.
        /// Follows major version increment specification (https://semver.org/#spec-item-8).
        /// </summary>
        /// <returns>The new incremented version.</returns>
        public SemanticVersion MajorIncremented() => new SemanticVersion(Major + 1, 0, 0, Prerelease, Build);

        /// <summary>
        /// Creates a new instance of <see cref="SemanticVersion"/> from the current instance, where the minor version has been incremented by 1.
        /// Follows minor version increment specification (https://semver.org/#spec-item-7).
        /// </summary>
        /// <returns>The new incremented version.</returns>
        public SemanticVersion MinorIncremented() => new SemanticVersion(Major, Minor + 1, 0, Prerelease, Build);

        /// <summary>
        /// Creates a new instance of <see cref="SemanticVersion"/> from the current instance, where the patch version has been incremented by 1.
        /// Follows patch version increment specification (https://semver.org/#spec-item-6).
        /// </summary>
        /// <returns>The new incremented version.</returns>
        public SemanticVersion PatchIncremented() => new SemanticVersion(Major, Minor, Patch + 1, Prerelease, Build);

        /// <summary>
        /// Gets a string representation of the current instance.
        /// </summary>
        /// <returns>The string representation.</returns>
        public override string ToString() => $"{Major}.{Minor}.{Patch}{Prerelease}{Build}";
    }
}
