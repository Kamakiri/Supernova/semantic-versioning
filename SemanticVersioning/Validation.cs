﻿using System.Text.RegularExpressions;

namespace Supernova.SemanticVersioning
{
    /// <summary>
    /// Provides validation for semantic version.
    /// </summary>
    public static class Validation
    {
        private const string IDENTIFIERS_PATTERN = @"(?:[0-9A-Za-z\-]+\.*)+";

        /// <summary>
        /// Gets a regular expression to validate semantic version build metadata.
        /// </summary>
        public static Regex BuildRegex { get; } = new Regex($@"\+{IDENTIFIERS_PATTERN}");

        /// <summary>
        /// Gets a regular expression to validate semantic version prerelease version.
        /// </summary>
        public static Regex PrereleaseRegex { get; } = new Regex($@"\-{IDENTIFIERS_PATTERN}");

        /// <summary>
        /// Gets a regular expression to validate semantic version.
        /// </summary>
        public static Regex SemanticVersionRegex { get; } = new Regex(SemanticVersionPattern);

        private static string SemanticVersionPattern => $@"^(?<major>0|[1-9]\d*)\.(?<minor>0|[1-9]\d*)\.(?<patch>0|[1-9]\d*)(?<prerelease>-{IDENTIFIERS_PATTERN})?(?<build>\+{IDENTIFIERS_PATTERN})?$";
    }
}
